package com.example.mvvm_pagingexample.single.movie.detail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.mvvm_pagingexample.R
import com.example.mvvm_pagingexample.data.api.POSTER_BASE_URL
import com.example.mvvm_pagingexample.data.api.TheMovieDbClient
import com.example.mvvm_pagingexample.data.api.TheMovieDbInterface
import com.example.mvvm_pagingexample.data.repository.MovieDetailsRepository
import com.example.mvvm_pagingexample.data.repository.NetworkState
import com.oxcoding.moviemvvm.data.vo.MovieDetails
import kotlinx.android.synthetic.main.activity_single_movie_detail.*
import java.text.NumberFormat
import java.util.*

class SingleMovieDetailActivity : AppCompatActivity() {

    private lateinit var viewModel: SingleMovieViewModel
    private lateinit var movieDetailsRepository: MovieDetailsRepository


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_movie_detail)

        val movieId:Int = intent.getIntExtra("id",1)
        val apiservice : TheMovieDbInterface =TheMovieDbClient.getClient()
        movieDetailsRepository = MovieDetailsRepository(apiservice)

        viewModel = getViewModel(movieId)

        viewModel.movieDetails.observe(this, Observer {
            bindUI(it)
        })

        viewModel.networkState.observe(this, Observer {
            progress_bar.visibility = if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility = if (it == NetworkState.ERROR) View.VISIBLE else View.GONE
        })
    }

    private fun bindUI(it: MovieDetails) {

        movie_title.text = it.title
        movie_tagline.text = it.tagline
        movie_release_date.text = it.releaseDate
        movie_rating.text = it.rating.toString()
        movie_runtime.text = it.runtime.toString() + " minutes"
        movie_overview.text = it.overview

        val formatCurrency = NumberFormat.getCurrencyInstance(Locale.US)
        movie_budget.text = formatCurrency.format(it.budget)
        movie_revenue.text = formatCurrency.format(it.revenue)

        val moviePosterURL = POSTER_BASE_URL + it.posterPath
        Glide.with(this)
            .load(moviePosterURL)
            .into(iv_movie_poster);
    }

    private fun getViewModel(movieId:Int):SingleMovieViewModel{
       return ViewModelProvider(this,object :ViewModelProvider.Factory{

           override fun <T : ViewModel?> create(modelClass: Class<T>): T {
               return SingleMovieViewModel(movieDetailsRepository,movieId)as T
           }
       })[SingleMovieViewModel::class.java]
    }
}
