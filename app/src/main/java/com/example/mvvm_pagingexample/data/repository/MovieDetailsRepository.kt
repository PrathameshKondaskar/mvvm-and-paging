package com.example.mvvm_pagingexample.data.repository

import androidx.lifecycle.LiveData
import com.example.mvvm_pagingexample.data.api.TheMovieDbInterface
import com.oxcoding.moviemvvm.data.vo.MovieDetails
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.internal.disposables.ListCompositeDisposable

class MovieDetailsRepository (private val apiservice : TheMovieDbInterface) {

    lateinit var movieDetailsNetworkDataSource: MovieDetailsNetworkDataSource
    fun fetchSingleMovieDetails (compositeDisposable: CompositeDisposable,movieId:Int):LiveData<MovieDetails>{

        movieDetailsNetworkDataSource = MovieDetailsNetworkDataSource(apiservice,compositeDisposable)
        movieDetailsNetworkDataSource.fetchMovieDetail(movieId)

        return movieDetailsNetworkDataSource.downloadMovieDetailResponse
    }
    fun getMovieDetailsNetworkState():LiveData<NetworkState>{
        return movieDetailsNetworkDataSource.networkState
    }
}