package com.example.mvvm_pagingexample.single.movie.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.mvvm_pagingexample.data.repository.MovieDetailsRepository
import com.example.mvvm_pagingexample.data.repository.NetworkState
import com.oxcoding.moviemvvm.data.vo.MovieDetails
import io.reactivex.disposables.CompositeDisposable

class SingleMovieViewModel(private val movieDetailsRepository: MovieDetailsRepository,movieId:Int) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val movieDetails : LiveData<MovieDetails> by lazy {
        movieDetailsRepository.fetchSingleMovieDetails(compositeDisposable,movieId)
    }

    val networkState : LiveData<NetworkState> by lazy {
        movieDetailsRepository.getMovieDetailsNetworkState()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}