package com.example.mvvm_pagingexample.data.api

import com.oxcoding.moviemvvm.data.vo.MovieDetails
import com.oxcoding.moviemvvm.data.vo.MovieResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheMovieDbInterface {


    //https://api.themoviedb.org/3/movie/419704?api_key=f9241ac5397236fff6afe274b25ee28c&language=en-US
    //https://api.themoviedb.org/3/movie/popular?api_key=f9241ac5397236fff6afe274b25ee28c&language=en-US&page=1

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id")id:Int):Single<MovieDetails>

    @GET("movie/popular")
    fun getPopularMovie(@Query("page")page:Int):Single<MovieResponse>

}